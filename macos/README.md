Currently, building the Apple Silicon and Universal schemes requires an ARM Mac on hand.

To get all dependencies, all you need to do is run `setup.command`.
Then you can open the Xcode project and build it.

If you would like to build the Steam target, you need to download [Steamworks](https://partner.steamgames.com/downloads/steamworks_sdk.zip). Run `setup.command` afterwards (it's important).